import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    for (const key in fighter) {
      switch (key) {
        case '_id':
          break;
        case 'source':
          const img_warpper = createElement({
            tagName: 'div',
            className: `fighter-preview___item fighter-preview___${ key }`,
          });
          img_warpper.appendChild(createFighterImage(fighter));
          fighterElement.appendChild(img_warpper);
          break;
        default:
          const tag = createElement({
            tagName: 'div',
            className: `fighter-preview___item fighter-preview___${ key }`,
          });
          tag.innerHTML = `<span class="name">${ key }:</span><span class="space">&nbsp;</span><span class="value">${ fighter[key] }</span>`;
          fighterElement.appendChild(tag);
          break;
      }
    }
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
