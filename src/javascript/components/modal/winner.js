import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper.js';

export function showWinnerModal(fighter) {
  // call showModal function
    const { name, source } = fighter;
    const title = `Winner: ${ name }`;
    const titleElement = createElement({
        tagName: 'p',
        className: 'modal-header___inner',
    });
    titleElement.textContent = title;
    const bodyElement = createElement({
        tagName: 'div',
        className: 'modal-body',
    });
    const image = createElement({
        tagName: 'img',
        className: 'modal-body___inner',
        attributes: {
            src: source,
            alt: title,
            title
        }
    });
    bodyElement.appendChild(image);

    return showModal({
        title: titleElement,
        bodyElement,
    });
}
