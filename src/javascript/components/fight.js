import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  function Fighter(initial, bar) {
    const { _id, name, attack, defense, health, source } = initial;
    this.bar = bar;
    this.ratio = 100;
    this._id = _id;
    this.name = name;
    this.attack = attack;
    this.defense = defense;
    this.currentHealth = health;
    this.health = health;
    this.source = source;

    this.updateBar = () => {
      this.bar.style.width = `${ this.ratio }%`;
    };

    this.updateHealth = (value) => {
      this.currentHealth = ((this.currentHealth - value) > 0) ? (this.currentHealth - value) : 0;
      this.ratio = (this.currentHealth / this.health) * 100;
      this.updateBar();
      return this.currentHealth;
    };

  }

  return new Promise((resolve) => {
    const firstFighterBar = document.getElementById('left-fighter-indicator');
    const secondFighterBar = document.getElementById('right-fighter-indicator');

    const playerOne = new Fighter(firstFighter, firstFighterBar);
    const playerTwo = new Fighter(secondFighter, secondFighterBar);
    const fighterArray = [playerOne, playerTwo];

    // Result condition
    const result = (arr) => {
      if (arr.some(i => {
        return !i.currentHealth;
      })) {
        return resolve(arr.filter(i => i.currentHealth)[0]);
      }
    };

    // Hits
    const ordinaryHit = (codes) => {
      const {
        PlayerOneAttack: p1a,
        PlayerTwoAttack: p2a,
        PlayerOneBlock:  p1b,
        PlayerTwoBlock:  p2b
      } = codes;
      let pressed = new Set();
      document.addEventListener('keydown', function(event) {
        pressed.add(event.code);
        if (pressed.has(p1a)) {
          if (!pressed.has(p1b) && !pressed.has(p2b)) {
            playerTwo.updateHealth(getDamage(playerOne, playerTwo));
          }
        }
        if (pressed.has(p2a)) {
          if (!pressed.has(p1b) && !pressed.has(p2b)) {
            playerOne.updateHealth(getDamage(playerTwo, playerOne));
          }
        }
        result(fighterArray);
      });
      document.addEventListener('keyup', function(event) {
        pressed.delete(event.code);
      });
    };
    const criticalHit = (flag, func, ...codes) => {
      let pressed = new Set();
      document.addEventListener('keydown', function(event) {
        pressed.add(event.code);
        for (let code of codes) {
          if (!pressed.has(code)) {
            return;
          }
        }
        pressed.clear();
        if (flag) {
          flag = false;
          func();
          setTimeout(() => { flag = true }, 10000);
        }
      });
      document.addEventListener('keyup', function(event) {
        pressed.delete(event.code);
      });
    };

    // resolve the promise with the winner when fight is over
    // Ordinary hit
    ordinaryHit(controls);
    // Critical hit for P1
    let flagP1 = true;
    criticalHit(flagP1, () => {
      playerTwo.updateHealth(2 * playerOne.attack);
      result(fighterArray);
    }, ...controls.PlayerOneCriticalHitCombination);
    // Critical hit for P2
    let flagP2 = true;
    criticalHit(flagP2, () => {
      playerOne.updateHealth(2 * playerTwo.attack);
      result(fighterArray);
    }, ...controls.PlayerTwoCriticalHitCombination);
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const attack = getHitPower(attacker);
  const defense = getBlockPower(defender);

  return (attack > defense) ? (attack - defense) : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = 1 + Math.random();
  const { attack } = fighter;

  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance  = 1 + Math.random();
  const { defense } = fighter;

  return defense * dodgeChance;
}
